from app import db, models
from sqlalchemy import desc
import datetime


class query(object):

    def select_last_events(self):
        sql = models.Events.query.order_by(desc('timestamp')).limit(10)
        return sql

    def count_events(self):
        sql = models.Events.query.count()
        return sql

    def select_sensor(self):
        sql = models.Sensor.query.all()
        return sql

    def count_sensors(self):
        sql = models.Sensor.query.count()
        return sql

    def count_triggers(self):
        sql = models.Switch.query.count()
        return sql

    def count_computers(self):
        sql = models.Computers.query.count()
        return sql

    def select_computers(self):
        sql = models.Computers.query.all()
        return sql
    def select_servers(self):
        sql = models.MyServers.query.all()
        return sql
    # Запись температуры в базу, в temperatureValue

    def add_temperature(self, id, value, time):
        sql = models.TemperatureValue(sensor_id=id, value=value, timestamp=time)
        db.session.add(sql)
        db.session.commit()

    # Запрос температуры для построения графиков

    def select_temperature(self):
        sql = models.TemperatureValue.query.order_by(desc('timestamp')).limit(100)
        return sql

    def select_single_sensor(self, sensor_id):
        sql = models.TemperatureValue.query.order_by(desc('timestamp')).filter_by(sensor_id=sensor_id).limit(20)
        return sql

    # Добавление нового сенсора в систему

    def add_new_sensor(self, type, w1_addr, place, desctiption, active):
        add = models.Sensor(type = type, w1_addr = w1_addr, place = place, desctiption = desctiption, active = active)
        db.session.commit()
        try:
            db.session.commit(add)
            return "Датчик " + type + " с адресом " + w1_addr + " успешно добавлен!"
        except Exception as error:
            return error

    def change_switch_status(self, place, status):
        sql = models.Switch.query.filter_by(place=place).first()
        sql.status = status
        db.session.add(sql)
        try:
            db.session.commit()
        except Exception as e:
            print("An error occurred" + str(e))

    def get_switch_status(self, place):
        sql = models.Switch.query.filter_by(place=place).first()
        status = sql.status
        return status

    def get_switch_pin(self, place):
        sql = models.Switch.query.filter_by(place=place).first()
        pin = sql.GPIO_pin
        return pin
    '''Удаление старых записей температур и влажности'''

    def delete_old_rows (self):
        too_old = datetime.datetime.today() - datetime.timedelta(days=1)

        models.TemperatureValue.query.filter(models.TemperatureValue.timestamp <= too_old).delete()
        models.HumidityValue.query.filter(models.HumidityValue.timestamp <= too_old).delete()
        db.session.commit()

    def select_last_temp(self, sensor_id):
        sql = models.TemperatureValue.query.order_by(desc('timestamp')).filter_by(sensor_id=sensor_id).first()
        last_temp = sql.value
        timestamp = sql.timestamp
        info = {'time': timestamp, 'temperature': last_temp}
        return info
