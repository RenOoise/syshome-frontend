from pyA20.gpio import gpio
import psutil
from datetime import timedelta
import platform
from pyA20.gpio import connector
from pyA20.gpio import port
from weather import Weather, Unit

''' Класс управления триггерами '''


class SwitchLights(object):
    def lights(self, pin, value):
        gpio.init()
        gpio.setcfg(pin, gpio.OUTPUT)
        gpio.output(pin, value)

    def ventilation(self, pin, value):
        gpio.init()
        gpio.setcfg(pin, gpio.OUTPUT)
        gpio.output(pin, value)

''' Класс сбора информации о сервере умного дома'''


class Server(object):
    def systemName(self):
        uname = platform.linux_distribution()
        system = {'os':  platform.system(), 'os.release': platform.release(), 'dist': uname[0], 'version': uname[1],
                  'version.name': uname[2], 'kernel': platform.platform()}
        return system

    def systemUptime(self):
        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
            uptime_string = str(timedelta(seconds=uptime_seconds))
            return uptime_string


class Devises(object):
    def ping(self, ip):

        return False


class TempOutside(object):
    def kaliningrad(self):
        weather = Weather(unit=Unit.CELSIUS)
        location = weather.lookup_by_location('kaliningrad')
        condition = location.condition
        weather = condition.temp

        return weather

    def perm(self):
        weather = Weather(unit=Unit.CELSIUS)
        location = weather.lookup_by_location('perm')
        condition = location.condition
        weather = condition.temp
        return weather


class Events(object):

    def new_event(self):
        return 0

    def last_event(self):
        return 0

    def get_last_events(self):

        return 0
