from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app
from flask_login import current_user, login_required
from flask_babel import get_locale
from app import db
from app.main.forms import EditProfileForm, SearchForm, MessageForm
from app.models import User, Post, Message, Notification, Sensor, TemperatureValue, Computers
from app.translate import translate
from app.main import bp
import redis
from app.main import classes, query
from flask import Response

import pygal
from sqlalchemy import desc
from pygal.style import Style

from flask_jsonpify import jsonify

redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)


@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()
    g.locale = str(get_locale())


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    # Graphs

    labels = []
    room = []
    kitchen = []
    bathroom = []
    hall = []
    hallway = []

    get_sensors = Sensor.query.all()

    for i in get_sensors:
        if i.place == 'room':
            query_values = TemperatureValue.query.filter_by(sensor_id=i.id).order_by(desc('id')).limit(20)
            for x in query_values:
                room.append(x.value)
                labels.append(datetime.strftime(x.timestamp, "%H:%M:%S"))
        elif i.place == 'kitchen':
            query_values = TemperatureValue.query.filter_by(sensor_id=i.id).order_by(desc('id')).limit(20)
            for x in query_values:
                kitchen.append(x.value)
        elif i.place == 'bathroom':
            query_values = TemperatureValue.query.filter_by(sensor_id=i.id).order_by(desc('id')).limit(20)
            for x in query_values:
                bathroom.append(x.value)
        elif i.place == 'hall1':
            query_values = TemperatureValue.query.filter_by(sensor_id=i.id).order_by(desc('id')).limit(20)
            for x in query_values:
                hall.append(x.value)
        elif i.place == 'hallway':
            query_values = TemperatureValue.query.filter_by(sensor_id=i.id).order_by(desc('id')).limit(20)
            for x in query_values:
                hallway.append(x.value)
    custom_style = Style( background='transparent', plot_background='transparent', opacity='.6',
                          colors=('#E853A0', '#E8537A', '#E95355', '#E87653', '#E89B53'),
                          foreground='#e8e3e3',
                          foreground_strong='#878585',
                          foreground_subtle='#c4c4c4', )
    graph = pygal.Line(style=custom_style)
    graph.title = 't*C'
    graph.x_labels = labels
    graph.add('Комната', room)
    graph.add('Кухня', kitchen)
    graph.add('Ванная', bathroom)
    graph.add('Прихожая', hallway)
    graph.add('Коридор', hall)
    graph_data = graph.render_data_uri()
    balcony_value = classes.TempOutside.kaliningrad(1)

    return render_template('index.html', title='Главная', index=True, graph_data=graph_data,
                           balcony_value=float(balcony_value))


@bp.route('/control')
def control():

    return render_template('control.html', control=True)


@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    posts = user.posts.order_by(Post.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.user', username=user.username,
                       page=posts.next_num) if posts.has_next else None
    prev_url = url_for('main.user', username=user.username,
                       page=posts.prev_num) if posts.has_prev else None
    return render_template('user.html', user=user, posts=posts.items,
                           next_url=next_url, prev_url=prev_url, title=user.name)


@bp.route('/user/<username>/popup')
@login_required
def user_popup(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template('user_popup.html', user=user)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.name = form.name.data
        current_user.about_me = form.about_me.data

        db.session.commit()
        flash('Ваши изменения были сохранены.')
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.name.data = current_user.name
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title='Редактирование профиля',
                           form=form)


@bp.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User %(username)s not found.', username=username)
        return redirect(url_for('main.index'))
    if user == current_user:
        flash('You cannot follow yourself!')
        return redirect(url_for('main.user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash('You are following %(username)s!', username=username)
    return redirect(url_for('main.user', username=username))


@bp.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User %(username)s not found.', username=username)
        return redirect(url_for('main.index'))
    if user == current_user:
        flash('You cannot unfollow yourself!')
        return redirect(url_for('main.user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash('You are not following %(username)s.', username=username)
    return redirect(url_for('main.user', username=username))


@bp.route('/translate', methods=['POST'])
@login_required
def translate_text():
    return jsonify({'text': translate(request.form['text'],
                                      request.form['source_language'],
                                      request.form['dest_language'])})


@bp.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.explore'))
    page = request.args.get('page', 1, type=int)
    posts, total = Post.search(g.search_form.q.data, page,
                               current_app.config['POSTS_PER_PAGE'])
    next_url = url_for('main.search', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('main.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title='Search', posts=posts,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/send_message/<recipient>', methods=['GET', 'POST'])
@login_required
def send_message(recipient):
    user = User.query.filter_by(username=recipient).first_or_404()
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(author=current_user, recipient=user,
                      body=form.message.data)
        db.session.add(msg)
        user.add_notification('unread_message_count', user.new_messages())
        db.session.commit()
        flash('Your message has been sent.')
        return redirect(url_for('main.user', username=recipient))
    return render_template('send_message.html', title='Send Message',
                           form=form, recipient=recipient)


@bp.route('/messages')
@login_required
def messages():
    current_user.last_message_read_time = datetime.utcnow()
    current_user.add_notification('unread_message_count', 0)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()).paginate(
            page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.messages', page=messages.next_num) \
        if messages.has_next else None
    prev_url = url_for('main.messages', page=messages.prev_num) \
        if messages.has_prev else None
    return render_template('messages.html', messages=messages.items,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/notifications')
@login_required
def notifications():
    since = request.args.get('since', 0.0, type=float)
    notifications = current_user.notifications.filter(
        Notification.timestamp > since).order_by(Notification.timestamp.asc())
    return jsonify([{
        'name': n.name,
        'data': n.get_data(),
        'timestamp': n.timestamp
    } for n in notifications])


@bp.route('/test')
def test():

    return render_template('test.html')


'''Обработчик страниц системы'''


@bp.route('/system')
@login_required
def system():
    system = classes.Server.systemName(1)
    uptime = classes.Server.systemUptime(1)
    sensors = query.query.count_sensors(1)
    triggers = query.query.count_triggers(1)
    events = query.query.count_events(1)
    number_of_computers = query.query.count_computers(1)
    computers = Computers.query.all()
    servers = query.query.select_servers(1)
    return render_template("system.html",
                           title='Устройства',
                           stats=True,
                           system=system,
                           uptime=uptime,
                           sensors=sensors,
                           triggers=triggers,
                           number_of_computers=number_of_computers,
                           computers=computers,
                           servers=servers,
                           events=events)


@bp.route('/map')
@login_required
def map():
    sensor = Sensor.query.all()

    def read_addr(place):
        for i in sensor:
            if i.place == place and i.type == "w1temp":
                sensor_addr = i.w1_addr
                ask_redis = redis_db.get(sensor_addr)
                return float(ask_redis)

    kitchen_temp = read_addr("kitchen")
    room_temp = read_addr("room")
    bathroom_temp = read_addr("bathroom")
    hall_temp = read_addr("hall")
    hallway_temp = read_addr("hallway")
    #bathroom_humidity = int(redis_db.get('hall_humidity'))

    weather = classes.TempOutside.kaliningrad(1)

    return render_template("map.html",
                           title='Схема квартиры',
                           map=True,
                           kitchen_temp=kitchen_temp,
                           room_temp=room_temp,
                           bathroom_temp=bathroom_temp,
                           hall_temp=hall_temp,
                           hallway_temp=hallway_temp,
                           #bathroom_humidity=bathroom_humidity,
                           weather=weather)


@bp.route('/room_values')
def room_values():
    sensor = Sensor.query.all()
    for i in sensor:
        if i.place == "room":
            sensor_addr = i.w1_addr

    def read_temp():
        ask_redis = redis_db.get(sensor_addr)
        temp = float(ask_redis)
        yield 'data: {0}\n\n'.format(temp)

    return Response(read_temp(), mimetype='text/event-stream')


@bp.route('/bathroom_values')
def bathroom_values():
    sensor = Sensor.query.all()
    for i in sensor:
        if i.place == "bathroom":
            sensor_addr = i.w1_addr

    def read_temp():
        ask_redis = redis_db.get(sensor_addr)
        temp = float(ask_redis)
        yield 'data: {0}\n\n'.format(temp)

    return Response(read_temp(), mimetype='text/event-stream')


@bp.route('/kitchen_values')
def kitchen_values():
    sensor = Sensor.query.all()
    for i in sensor:
        if i.place == "kitchen":
            sensor_addr = i.w1_addr

    def read_temp():
        ask_redis = redis_db.get(sensor_addr)
        temp = float(ask_redis)
        yield 'data: {0}\n\n'.format(temp)

    return Response(read_temp(), mimetype='text/event-stream')


@bp.route('/hallway_values')
def hallway_values():
    sensor = Sensor.query.all()
    for i in sensor:
        if i.place == "hallway":
            sensor_addr = i.w1_addr

    def read_temp():
        ask_redis = redis_db.get(sensor_addr)
        temp = float(ask_redis)
        yield 'data: {0}\n\n'.format(temp)

    return Response(read_temp(), mimetype='text/event-stream')


@bp.route('/hall_values')
def hall_values():
    sensor = Sensor.query.all()
    for i in sensor:
        if i.place == "hall":
            sensor_addr = i.w1_addr

    def read_temp():
        ask_redis = redis_db.get(sensor_addr)
        temp = float(ask_redis)
        yield 'data: {0}\n\n'.format(temp)

    return Response(read_temp(), mimetype='text/event-stream')


@bp.route('/balcony_values')
def balcony_values():
    weather = classes.TempOutside.kaliningrad(1)

    def read_temp():
        temp = float(weather)
        yield 'data: {0}\n\n'.format(temp)

    return Response(read_temp(), mimetype='text/event-stream')


@bp.route('/switch/room/<int:state>', methods=['POST'])
@login_required
def room_switch(state):

    s = 1