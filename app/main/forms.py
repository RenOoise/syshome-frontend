from flask import request
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import ValidationError, DataRequired, Length, IPAddress
from app.models import User, Sensor


class EditProfileForm(FlaskForm):
    username = StringField('Логин', validators=[DataRequired()])
    name = StringField('Имя')
    about_me = TextAreaField('О себе',
                             validators=[Length(min=0, max=140)])
    submit = SubmitField('Подтвердить')

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError('Please use a different username.')


class PostForm(FlaskForm):
    post = TextAreaField('Say something', validators=[DataRequired()])
    submit = SubmitField('Submit')


class SearchForm(FlaskForm):
    q = StringField('Search', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)


class MessageForm(FlaskForm):
    message = TextAreaField('Message', validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField('Submit')


class AddSensorForm(FlaskForm):
    w1_addr = StringField('1-wire адрес', validators=[DataRequired()])
    type = StringField('Тип', validators=[DataRequired()])
    function = StringField('Функция', validators=[DataRequired()])
    place = StringField('Место установки', validators=[DataRequired()])
    submit = SubmitField('Добавить')

    def validate_w1_addr(self, w1_addr):
        user = Sensor.query.filter_by(w1_addr=w1_addr.data).first()
        if user is not None:
            raise ValidationError('Датчик с таким адресом уже существует!')


class AddComputerForm(FlaskForm):
    name = StringField('Имя', validators=[DataRequired()])
    type = StringField('Тип', validators=[DataRequired()])
    ip_address = StringField('Айпи адрес', validators=[DataRequired(), IPAddress(ipv4=True, ipv6=False,
                                                                                     message=None)])
    place = StringField('Место нахождения')
    mac_address = StringField('Мак адрес', validators=[DataRequired()])
    #   owner = StringField('Владелец', validators=[DataRequired()])
    submit = SubmitField('Добавить')


class TestForm(FlaskForm):

    time = StringField('Время сканирования &nbsp; &nbsp; ', render_kw={"data-slider-min": "1",
                                                                       "data-slider-max": "10",
                                                                       "data-slider-step": "1",
                                                                       "data-slider-value": "1"}, id="ex6")
    submit = SubmitField('Применить')
