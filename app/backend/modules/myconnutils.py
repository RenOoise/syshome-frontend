import pymysql.cursors


# Connect to the database
def getConnection():
    connection = pymysql.connect(host='localhost',
                                 user='syshome',
                                 password='eb5u8sop',
                                 db='syshome',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection
