from app.backend.modules import myconnutils, sensors
import redis
from app import db
from app.models import TemperatureValue, Sensor
from datetime import datetime, timedelta
from time import sleep

redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)


# функция удаления старых записей
def delete_older_rows():
    try:
        sql = TemperatureValue.query.filter_by(TemperatureValue.timestamp < datetime.today() - timedelta(days=7)).all()
        db.session.delete(sql)
        db.session.commit()
    finally:
        print('Таблица очищена')


def temp():

    sensors = Sensor.query.filter_by(type='w1temp', function='temp').all()
    for i in sensors:
        id = i.id
        address = i.w1_addr
        temp = sensors.w1.w1_temp(1, address)
        timestamp = datetime.now()
        if temp is not None:
            if float(temp) < float(50):
                redis_db.set(address, temp)
            ask_redis = redis_db.get(address)
            if float(ask_redis) == float(temp):
                sql = TemperatureValue(sensor_id=id, value=temp, timestamp=timestamp)
                db.session.add(sql)
                db.session.commit()
                print('Добавлено значение для сенсора с адресом ', address, temp, ' градусов')
        else:
            print('Сенсор с адресом ', address, ' отвалился или не отвечает')




