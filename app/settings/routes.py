from flask import render_template, flash, redirect, url_for, request, jsonify, current_app
from flask_login import current_user, login_required
from app import db
from app.settings.forms import DeleteSensorForm, EditSensorForm, EditSensorButtonForm, ActivateSensorForm,\
    DeactivateSensorForm, AddSwitchForm
from app.main.forms import TestForm, AddSensorForm, AddComputerForm
from app.models import Sensor, Computers, Settings, Switch
from app.settings import bp
import redis

redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)

'''Обработчик страниц конфигуратора'''


# Страница добавления нового сенсора
@bp.route('/settings/add_sensor', methods=['GET', 'POST'])
@login_required
def add_sensor():
    form = AddSensorForm()
    if form.validate_on_submit():
        sensor = Sensor(w1_addr=form.w1_addr.data, type=form.type.data, function=form.function.data,
                        place=form.place.data)
        db.session.add(sensor)
        db.session.commit()
        flash('Датчик  успешно добавлен!')
        return redirect(url_for('settings.sensors'))
    return render_template('settings/add_sensor.html', title='Добавить датчик',
                           form=form)


# Страница добавления нового переключателя
@bp.route('/settings/add_switch', methods=['GET', 'POST'])
@login_required
def add_switch():
    form = AddSwitchForm()
    if form.validate_on_submit():
        switch = Switch(pin=form.pin.data, type=form.type.data, function=form.function.data,
                        place=form.place.data, desctiption=form.description.data)
        db.session.add(switch)
        db.session.commit()
        flash('Датчик  успешно добавлен!')
        return redirect(url_for('settings.switches'))
    return render_template('settings/add_switch.html', title='Добавление нового переключателя',
                           form=form)


# Главная страница настроек
@bp.route('/settings', methods=['GET', 'POST'])
@login_required
def configuration():
    form = TestForm()
    time = Settings.query.filter_by(option='TempUpdateTime').first_or_404()
    print(time.int_value)
    if time is None:
        time.int_value = 0
        db.session.commit()
    if form.validate_on_submit():
        time.int_value = form.time.data
        db.session.commit()
        print('committed')
        return redirect(url_for('main.configuration'))
    return render_template("settings/configuration.html",
                           title='Настройки / Главная',
                           configuration=True, form=form, timevalue=time.int_value)


# Страница датчиков
@bp.route('/settings/sensors', methods=['GET', 'POST'])
@login_required
def sensors():
    tableItems = Sensor.query.all()
    formdel = DeleteSensorForm()
    formedit = EditSensorButtonForm()
    formactive = ActivateSensorForm()
    formdeactivate = DeactivateSensorForm()

    return render_template('settings/sensors.html',
                           title='Настройки / Датчики',
                           configure_sensors=True,
                           tableItems=tableItems,
                           formdel=formdel,
                           formedit=formedit,
                           formactive=formactive,
                           formdeactivate=formdeactivate)


# Страница переключателей
@bp.route('/settings/switches', methods=['GET', 'POST'])
@login_required
def switches():
    tableItems = Switch.query.all()
    formdel = DeleteSensorForm()
    formedit = EditSensorButtonForm()
    formactive = ActivateSensorForm()
    formdeactivate = DeactivateSensorForm()

    return render_template('settings/switches.html',
                           title='Настройки / Переключатели',
                           configure_sensors=True,
                           tableItems=tableItems,
                           formdel=formdel,
                           formedit=formedit,
                           formactive=formactive,
                           formdeactivate=formdeactivate)


# Страница редактирования свойств сенсора. Выбор по <sensor.id>
@bp.route('/settings/sensors/edit/<sensor_id>', methods=['GET', 'POST'])
@login_required
def edit_sensor(sensor_id):
    sensor = Sensor.query.filter_by(id=sensor_id).first_or_404()
    form = EditSensorForm(obj=sensor)

    if form.validate_on_submit():
        sensor.w1_addr = form.w1_addr.data
        sensor.GPIO_pin = form.pin.data
        sensor.function = form.function.data
        sensor.place = form.place.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('settings.sensors'))

    elif request.method == 'GET':
        form.w1_addr.data = sensor.w1_addr
        form.pin.data = sensor.GPIO_pin
        form.function.data = sensor.function
        form.place.data = sensor.place

    return render_template('/settings/edit_sensor.html',
                           form=form,
                           title='Редактирование свойств датчика')


# Страница удаления сенсора. Выбор по <sensor.id>
@bp.route('/settings/sensors/delete/<sensor_id>', methods=['POST'])
@login_required
def delete_sensor(sensor_id):
    sensor = Sensor.query.filter_by(id=sensor_id).one()
    db.session.delete(sensor)
    db.session.commit()
    return redirect(url_for('settings.sensors'))


# Страница ацтивации сенсора. Выбор по <sensor.id>
@bp.route('/settings/sensors/activate/<sensor_id>', methods=['POST'])
@login_required
def activate_sensor(sensor_id):
    sensor = Sensor.query.filter_by(id=sensor_id).first_or_404()
    sensor.active = True
    db.session.commit()
    return redirect(url_for('settings.sensors'))


# Страница деактивации сенсора. Выбор по <sensor.id>
@bp.route('/settings/sensors/deactivate/<sensor_id>', methods=['POST'])
@login_required
def deactivate_sensor(sensor_id):
    sensor = Sensor.query.filter_by(id=sensor_id).first_or_404()
    sensor.active = False
    db.session.commit()
    return redirect(url_for('settings.sensors'))


@bp.route('/add_device', methods=['GET', 'POST'])
def add_device():
    form = AddComputerForm()

    if form.validate_on_submit():
        sql = Computers(type=form.type.data, ip_address=form.ip_address.data, hostname=form.name.data,
                        place=form.place.data, mac_address=form.mac_address.data)
        db.session.add(sql)
        db.session.commit()
        flash('Устройство успешно добавлено')
        return redirect(url_for('main.system'))
    return render_template('add_device.html', title='Добавление нового устройства',
                           form=form)
