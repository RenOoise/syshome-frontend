from flask_wtf import FlaskForm
from wtforms import SubmitField, StringField
from wtforms.validators import DataRequired


class DeleteSensorForm(FlaskForm):
    submit = SubmitField('Удалить')


class EditSensorButtonForm(FlaskForm):
    submit = SubmitField('Редактировать')


class EditSensorForm(FlaskForm):
    w1_addr = StringField('1-wire адрес', validators=[DataRequired()])
    pin = StringField('Пин', validators=[DataRequired()])
    function = StringField('Функция', validators=[DataRequired()])
    type = StringField('Тип сенсора', validators=[DataRequired()])
    place = StringField('Место', validators=[DataRequired()])
    submit = SubmitField('Подтвердить')


class ActivateSensorForm(FlaskForm):
    submit = SubmitField('Активировать')


class DeactivateSensorForm(FlaskForm):
    submit = SubmitField('Деактивировать')


''' Переключатели '''


class AddSwitchForm(FlaskForm):
    pin = StringField('Пин', validators=[DataRequired()])
    function = StringField('Функция', validators=[DataRequired()])
    type = StringField('Тип переключателя', validators=[DataRequired()])
    place = StringField('Место', validators=[DataRequired()])
    description = StringField('Описание', validators=[DataRequired()])
    submit = SubmitField('Подтвердить')
